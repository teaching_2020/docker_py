
FROM python:3.7

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

ENV DEMO_ALLOWED_HOSTS 127.0.0.1

COPY . .

CMD [ "gunicorn", "demo.wsgi:application",  "--bind", "0.0.0.0:8000"]