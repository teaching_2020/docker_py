
# Django App And Docker

The following tutorial explains how to set up a docker containe rfor developing a django app, how to build it and how to deploy the docker container to Azure.

**Prerequisites:** you should get an Azure subscription. 

## Development environment with docker


### Set up the docker container
First create a project folder (for example `demo`), open a console and **change the working dir to the project folder** you have created.

Create a docker container with:

```
# docker run -d -p 8080:8000 -v ${PWD}:/usr/src/app --name python_demo -it python:3.7
```

* `-p 8000:8000` this maps the internal port 8000 to our local port 8080. We will start the django app on port 8000 in docker, and we will access it under [http://localhost:8080](http://localhost:8080) locally
* `-v ${PWD}:/usr/src/app` this maps the current working dir to `/usr/src/app` in the container
* `-it python:3.7` this indicates the source for our docker container image [https://hub.docker.com/_/python](https://hub.docker.com/_/python)


This should have created a docker image for you, you should see it executing `docker ps -a`:

```
# docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                     PORTS                      NAMES
6d618b3635f9        python:3.7          "python3"                5 minutes ago       Up 5 minutes               0.0.0.0:8000->8000/tcp     python_demo
```

To start / stop the docker container, execute the following (where the last parameter is the image id listed `by docker ps`):

```
# docker start <put the ID here>
# docker stop <put the ID here>
```

To open a console into the docker container::

```
# docker exec -it python_demo bash
```


### Install the packages you need in the docker console
Open a console in docker (`docker exec -it python_demo bash`). We will use Django as the web framework and gunicorn for firing up the django app later on.


```
root@b955cd526130:/# pip install Django gunicorn
```

### Create the app
Still within the docker console, will create a django project in the folder `/usr/src/app`.

**Note:** The path `/usr/src/app` is the destination to which we mapped our local `demo`. Any files you add or change here will be found in the `demo` folder of your project.

```
root@b955cd526130:/# cd /usr/src/app
root@b955cd526130:/usr/src/app# django-admin startproject demo .
``` 

Now should have created the django project, so check that out. The `ls` command should shown you the content of the folder. 

```
root@b955cd526130:/usr/src/app# ls
demo  manage.py
```

Now fire app the application. We will start our app on the network interface 0.0.0.0 and port 8000 (this is the port that we mapped to the outside with `-p 8000:8000` when starting docker)

```
root@b955cd526130:/usr/src/app# python manage.py runserver 0.0.0.0:8000
...

March 18, 2020 - 10:52:45
Django version 3.0.4, using settings 'demo.settings'
Starting development server at http://0.0.0.0:8000/
Quit the server with CONTROL-C.

```

Now open a browser and navigate to [http://127.0.0.1:8080](http://127.0.0.1:8080) .


### Running the app in docker
You can start the python app directly from your local console (without opening explicitly a docker console first):

```
# docker exec -it python_demo python /usr/src/app/manage.py runserver 0.0.0.0:8000
```

### Coding the app
Since the project folder folder is mapped to `/usr/src/app` in the docker container, any change you make is immedialtly reflected in docker.

HAPPY CODING !!!

## Building the docker container

In order to deploy the coker container you should first build it.
First make sure all the dependences are listed in the file `requirements.txt` (running `pip freeze` in docker will output you all installed packages. For this simple demo the requirements file should be:

```
Django==3.0.4
gunicorn==20.0.4
```

Create a `Dockerfile` with the following content:

```
FROM python:3.7

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "gunicorn", "demo.wsgi:application",  "--bind", "0.0.0.0:8000"]
```
 
Build the docker container

```
docker build -t demo_app .
```

Test the build, run the following command and then check the browser ([http://127.0.0.1:8081](http://127.0.0.1:8081))

```
docker run --rm -p 8081:8000 -it demo_app
```

# Deployment

## Create an container registry
A container registry is a repozitory that holds the build docker conteiners.
Follow these steps to create one:

![Step_1](./images/Step_1.png)
![Step_2](./images/Step_2.png)
![Step_3](./images/Step_3.png)
![Step_4](./images/Step_4.png)
![Step_5](./images/Step_5.png)
![Step_6](./images/Step_6.png)

Under access keys you can check for the user & password that you will need when logging into the registry before pushing the container:

![Step_7](./images/Step_7.png)



## Install Azure CLI & push the container

Follow the instructions [here](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest).

Follow the instructions for pushing the docker build from [here](https://docs.microsoft.com/en-us/azure/container-registry/container-registry-get-started-docker-cli)

Login to azure from command line. Execute the following command and follow the instructions:

```
az login
az acr login --name geralddemoregistry
docker tag demo_app geralddemoregistry.azurecr.io/samples/demo_app
docker push geralddemoregistry.azurecr.io/samples/demo_app
```

You should be able to see the pused container in the registry on azure:

![Step_8](./images/Step_8.png)


## Create a an instance of the container

Follow the steps ilustrated below to create in instance of the container:

![Step_9](./images/Step_9.png)
![Step_10](./images/Step_10.png)
![Step_11](./images/Step_11.png)
![Step_12](./images/Step_12.png)
![Step_13](./images/Step_13.png)
![Step_14](./images/Step_14.png)

The container instance should be have been created.
Open the page for the instance details. Look for the IP address, copy it and paste it in the browser like: `https://<ip adress>:8000`. You sould see your web page.

![Step_15](./images/Step_15.png)


