"""demo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('random_number/', include('random_number.urls')),
]

import logging
from opencensus.ext.azure.log_exporter import AzureLogHandler

log = logging.getLogger(__name__)

# TODO: replace the all-zero GUID with your instrumentation key.
log.addHandler(AzureLogHandler(
    connection_string='InstrumentationKey=7274a1e9-c5ca-4052-a0b9-61a3c00cb2da')
)

# print("--- 1")
# log.error("Hello World !!!")
# print("--- 2")
# log.info("Hello World !!!")
# print("--- 3")
# log.warn("Hello World !!!")
# print("--- 4")
# log.debug("Hello World !!!")
# print("--- 5")
# log.critical("Hello World !!!")
# print("--- 6")
