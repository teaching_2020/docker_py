from django.shortcuts import render
import random
import logging
from opencensus.ext.azure.log_exporter import AzureLogHandler
from django.http import HttpResponse
import time
from django.views.decorators.csrf import csrf_exempt
from .models import NumberGenerationJob

log = logging.getLogger(__name__)

# Create your views here.
@csrf_exempt
def index(request):
    print(request.POST["callback_url"])

    job = NumberGenerationJob(
        callback_url=request.POST["callback_url"],
        status="PENDING"
    )
    job.save()

    return HttpResponse(job.id)

 