from django.db import models

# Create your models here.

class NumberGenerationJob(models.Model):
    status = models.CharField(max_length=20)
    callback_url = models.CharField(max_length=200, null=True)
    result = models.IntegerField(null=True)

