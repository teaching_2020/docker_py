from django.core.management.base import BaseCommand
from django.utils import timezone
import time
from random_number.models import NumberGenerationJob
import random
import requests

class Command(BaseCommand):
    help = 'Run jobs from the DB'

    def handle(self, *args, **kwargs):
        self.stdout.write("Running jobs")
        while True:
            job_list = NumberGenerationJob.objects.filter(status="PENDING")
            print(job_list)
            for job in job_list:
                job.result = random.randrange(1000000)
                job.status = "DONE"
                job.save()

                r = requests.get(job.callback_url, params={"job_id": job.id, "result": job.result })
                print(r)
                
            time.sleep(1)